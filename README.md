Enlace a github: **https://github.com/agcdam/practica-2.git**

# Instrucciones para la correcta ejecución de los programas.
## Ejecutar ejercicio 1.
Ejecuta el comando si estas *en Linux*  
```bash
java -jar Ejercicio1.jar {COMANDO}
```
Si estas en windows usa 
```bash
java -jar Ejercicio1.jar cmd.exe /c {COMANDO}
```
La salida del programa se escribira en un fichero generado en la carpeta llamado **output.txt**

## Ejecutar ejercicio 2.
Ejecuta el comando  
```bash
java -jar Ejercicio2.jar
```
La salida del proces se mostrara en el fichero **randoms.txt**

### Ejecutar el programa Random10.java por separado
Ejecuta el comando
```bash
java -jar Random10.jar
```

## Ejecutar ejercicio 3.
Ejecuta el comando
```bash
java -jar Ejercicio3.jar
```

### Ejecutar el programa Minusculas.java por separado
Ejecuta el comando
```bash
java -jar Minusculas.jar
```