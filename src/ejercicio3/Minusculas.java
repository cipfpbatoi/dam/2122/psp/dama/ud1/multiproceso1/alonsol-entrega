package ejercicio3;

//Importamos las dependencias necesarias para el correcto funcionamiento
import java.util.Scanner;

public class Minusculas {
    public static void main(String[] args) {
        //Inicializamos el Scanner de entrada
        Scanner sc = new Scanner(System.in);
        String linea = sc.nextLine();

        //Mostramos la salida del proceso mientras no escribamos "finalizar"
        while (!linea.toLowerCase().equals("finalizar")) {
            System.out.println(linea.toLowerCase());
            linea = sc.nextLine();
        }

        //Cerramos el buffer
        sc.close();
    }
}
