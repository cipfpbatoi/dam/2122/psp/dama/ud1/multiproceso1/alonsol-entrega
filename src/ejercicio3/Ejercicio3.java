package ejercicio3;

//Importacion de las dependencias necesarias
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Ejercicio3 {

    public static void main(String[] args) {
        //Convertimos el comando necesario para ejecutar Minusculas.jar en una lista de Strings
        String comando = "java -jar Minusculas.jar";
        List<String> comList = new ArrayList<>(Arrays.asList(comando.split(" ")));

        //Inicializamos el proceso hijo con el comando convertido en una lista de Strings
        ProcessBuilder pb = new ProcessBuilder(comList);

        try {
            //Inicialiazamos el proceso
            Process process = pb.start();
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));

            //Leyemos la salida del proceso
            Scanner prec = new Scanner(process.getInputStream());

            //Leyemos la entrada de texto
            Scanner sc = new Scanner(System.in);
            String linea = sc.nextLine();
            String num;
            
            //Bucle donde nos comunicamos con el proceso hijo hasta escribir la palabra "finalizar"
            while (!linea.toLowerCase().equals("finalizar")) {
                bw.write(linea);
                bw.newLine();
                bw.flush();
                num = prec.nextLine();
                System.out.println(num);
                linea = sc.nextLine();
            }

            //Cerramos el buffer
            sc.close();

        //Excepciones
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    
    
}
