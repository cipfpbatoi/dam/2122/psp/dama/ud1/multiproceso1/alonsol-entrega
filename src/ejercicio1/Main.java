package ejercicio1;

//Importaciones necesarias para el correcto funcionamiento
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import excepcions.TimeOutException;

public class Main {

    //Constante del tiempo de espera
    private static final long TIEMPO_ESPERA = 2;

    public static void main(String[] args) {

        //Convertimos los argumentos del programa en una lista de Strings
        /**
         * Se podría realizar utilizando otras funcionalidades más
         * directas o incluso con un foreach, pero funciona!
         */
        List<String> command = new ArrayList<>();
        for (int i = 0; i < args.length; i++) {
            command.add(args[i]);
        }

        //Inicializamos el proceso hijo pasandole los argumentos del Main convertidos en la lista
        ProcessBuilder pb = new ProcessBuilder(command);
        InputStream dir;

        try {
            //Hacemos que el proceso hijo espere 2 segundos antes de finalizarlo
            Process process = pb.start(); 
            boolean t = process.waitFor(TIEMPO_ESPERA, TimeUnit.SECONDS);
            if (!t) {
                throw new TimeOutException("Tiempo finalizado");
            }

            /**
             * Se tendría que haber obtenido el resultado del proceso,
             * para saber si leer o no la salida estándar o la de error.
             * Porque en el caso de que termine con error, se ha de leer la 
             * salida estándar
             */


            //Inicia el proceso y lee la salida del mismo
            dir = pb.start().getInputStream();
            InputStreamReader isr = new InputStreamReader(dir);
            BufferedReader br = new BufferedReader(isr); 
            Scanner sc = new Scanner(br);

            //Inicializamos el Buffer para escribir en un fichero
            File f = new File("output.txt");
            BufferedWriter bw = new BufferedWriter(new FileWriter(f));
            
            //Escribimos la salida del proceso dentro del fichero
            /**
             * Si Se utiliza bufferedwriter, no se tiene que utilizar
             * el salto de línea porque depende de la plataforma
             * y al utilizar BufferedWritter, quedarí así:
             */
            while (sc.hasNext()) {
                bw.write(sc.nextLine());
                bw.newLine();
            }

            //Cerramos todos los buffers
            /**
             * Ideal cerrarlo dentro de un finally o mejor 
             * utilizando la sintaxis try () {} que os
             * explicará Sergi.
             */
            bw.close();
            sc.close();
            
        //Excepciones
        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        } catch (InterruptedException e) {
            System.out.println("Error: " + e.getMessage());
        } catch (TimeOutException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}
