package ejercicio2;

//Importamos las dependencias necesarias para el correcto funcionamiento
import java.util.Scanner;

public class Random10 {
    public static void main(String[] args) {
        
        //Inicializamos el Scanner de entrada
        Scanner sc = new Scanner(System.in);

        //Creamos el bucle que nos premite mostrar numeros mientras no pongas la palabra "stop"
        while (!sc.nextLine().equals("stop")) {
            int num = Random();
            System.out.println(num);
        }

        //Cerramos el buffer abierto
        sc.close();
    }


    //Metodo que nos devuelve un numero aleatorio
    /**
     * Método que devuelve un número entero entre 0 y 10
     * @return Un entero entre 0 y 10
     */
    private static int Random() {
        return (int) (Math.random() * 10 + 1);
    }
}
