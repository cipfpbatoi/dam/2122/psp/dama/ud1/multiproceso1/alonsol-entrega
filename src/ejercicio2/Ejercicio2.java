package ejercicio2;

//Importacion de las dependencias necesarias
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Ejercicio2 {
    public static void main(String[] args) {
        //Convertimos el comando necesario para ejecutar Random10.jar en una lista de Strings
        String comando = "java -jar Random10.jar";
        List<String> comList = new ArrayList<>(Arrays.asList(comando.split(" ")));

        //Inicializamos el proceso hijo con el comando convertido en una lista de Strings
        ProcessBuilder pb = new ProcessBuilder(comList);

        try {
            //Inicializamos el proceso
            Process process = pb.start();
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));

            //Leyemos la salida del proceso
            Scanner prec = new Scanner(process.getInputStream());

            //Leyemos la entrada de texto
            Scanner sc = new Scanner(System.in);
            String linea = sc.nextLine();
            String num;

            //Inicializamos el fichero donde se van a guardar los resultados
            File f = new File("randoms.txt");
            BufferedWriter bwr = new BufferedWriter(new FileWriter(f));

            //Bucle donde nos comunicamos con el proceso hijo hasta escribir la palabra "stop"
            while (!linea.equals("stop")) {
                bw.write(linea);
                bw.newLine();
                bw.flush();
                num = prec.nextLine();
                System.out.println(num);
                linea = sc.nextLine();
                /**
                 * Lo mismo que en el ejercicio1
                 * 
                 * Recomiendo hacer un flush de vez en cuando
                 * por si acaso salta una excepción, no tendríamos 
                 * nada en el fichero.
                 * 
                 */
                bwr.write(num);
                bwr.newLine();
            }

            //Cerramos los buffers
            /**
             * Mejor cerrar los buffers en un finally
             */
            bwr.close();
            sc.close();

        //Captamos las excepciones
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
    }
}
